/**
 * Created by techno23 on 11/12/13.
 */
function uuid() {
    chars='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var result = '';
    for (var i = 8; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    result+='-';
    for (var j = 0; j < 3; j++){
        for (var i = 4; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
        result+='-';
    }
    for (var i = 12; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}
